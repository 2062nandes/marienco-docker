// Via CommonJS
// Sticky-js
// const Sticky = require('sticky-js/dist/sticky.min')

// Via AMD
import { wowjs } from './libraries/wow'
import { tns } from './libraries/tiny'
import { gallerybox } from './libraries/baguettebox'

import { menuToggle } from "./modules/menu";
import { getdate } from "./modules/date";

// Data intancia Vue
import { contactform } from './data/contactform'
import { menuinicio, mainmenu } from './data/menus'
import { represents } from './data/represents'

import Vue from 'vue/dist/vue.min'
// import Vue from 'vue/dist/vue'
import VueResource from 'vue-resource/dist/vue-resource.min'
Vue.use(VueResource);

// Vue Components
import { googleMap } from './components/googlemaps'
Vue.component('google-map', googleMap)

// Vue instancia
const vm = new Vue({
  el: '#app',
  data: {
    path_page: '/',
    path_media: '/assets/',
    formSubmitted: false,
    vue: contactform,
    menuinicio,
    mainmenu,
    represents
  },
  created: () => {
    wowjs()
    getdate()
  },
  mounted: function () {
    let slider = tns({
      container: '.carousel-slider',
      speed: 1200,
      autoplay: true,
      autoplayTimeout: 2500,
      autoplayHoverPause: true,
      responsive: {
        "0": {
          items: 2
        },
        "640": {
          items: 3  
        },
        "950": {
          items: 4
        },
        "1124": {
          items: 5
        },
        "1240": {
          items: 6
        },
        "1440": {
          items: 7
        }
      },
    });
    gallerybox()
    menuToggle()
  },
  methods: {
    isFormValid: function () {
      return this.nombre != ''
    },
    clearForm: function () {
      this.vue.nombre = ''
      this.vue.email = ''
      this.vue.telefono = ''
      this.vue.movil = ''
      this.vue.direccion = ''
      this.vue.ciudad = ''
      this.vue.mensaje = ''
      this.vue.formSubmitted = false
    },
    submitForm: function () {
      if (!this.isFormValid()) return
      this.formSubmitted = true
      this.$http.post('../../mail.php', { vue: this.vue }).then(function (response) {
        this.vue.envio = response.data
        this.clearForm()
      }, function () { })
    },
    acordionVue: function (id) {
      if (this.mainmenu[id].state == true) {
        this.mainmenu[id].state = false
      } else {
        for (let i = 0; i < this.mainmenu.length; i++) {
          this.mainmenu[i].state = false;
        }
        this.mainmenu[id].state = true;
      }
    }
  }
})
