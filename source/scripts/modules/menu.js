export const menuToggle = (toggleId, navId) => {
  const menu = (toggleId, navId) => {
    const toggle = document.getElementById(toggleId),
    nav = document.getElementById(navId);
    if (toggle && nav) {
      toggle.addEventListener('click', () => {
        nav.classList.toggle('show');
        if (navId === 'main-menu') document.body.classList.toggle('main-menu-visible')
      })
    }
  }
  const removemenu = (toggleId, navId) => {
    const toggle = document.getElementById(toggleId),
    nav = document.getElementById(navId);
    document.body.classList.remove('main-menu-visible')
    nav.classList.remove('show')
  }
  const mediumBp = matchMedia('(min-width: 1023px)')
  const changeSize = mql => { //mediaquery list
    mql.matches ? removemenu('main-menu-toggle', 'main-menu') : ''
  }
  menu('main-menu-toggle', 'main-menu')
  mediumBp.addListener(changeSize)
  changeSize(mediumBp)
  
  menu('vertical-menu-toggle', 'vertical-menu');

  // const activeMenuItem = containerId => {
  //   let links = [...document.querySelectorAll(`#${containerId} #nds`)];
  //   const curentUrl = document.location.href;
  //   links.map(link => {
  //     if (link.href === curentUrl) {
  //       link.classList.add('active')
  //     }
  //   });
  // };
  // activeMenuItem('vertical-menu');
};
