export const menuinicio = [
  {
    title: 'Inicio',
    href: '',
    icon: 'icon-home',
    wow_delay: '0.3s'
  },
  {
    title: 'Nosotros',
    href: 'nosotros.html',
    icon: 'icon-us',
    wow_delay: '0.5s'
  },
  {
    title: 'Contactos',
    href: 'contactos.html',
    icon: 'icon-contact',
    wow_delay: '0.7s'
  }
]

export const mainmenu = [
  {
    title: 'Geosintéticos',
    href: 'geosinteticos',
    state: false,
    submenu: [      
      {
        title: 'Geomembranas',
        href: 'geomembranas',
        src: 'images/geomembranas-400px.jpg',
        subsubmenu: [
          {
            title: 'Geotextiles',
            src: 'images/no_imagen.png', 
            href: '',
            description: `<p>Utilizadas para la impermeabilización de grandes áreas, las geomembranas de PVC poseen espesores homogéneos, libres de imperfecciones, son tolerantes a la acción de ácidos, bases y sales, además de resistentes al envejecimiento e influencias del ambiente.  Pueden ser aplicadas sobre el suelo, gracias a su maleabilidad y flexibilidad.</p>
                          <p>Con todas estas cualidades, las geomembranas de PVC son una opción versátil y viable para su utilización en diversas aplicaciones como os segmentos, como Impermeabilización de represas agrícolas, piletas para piscicultura, canales de riego, estanques de tratamiento de residuos, estanques de concentración de residuos de procesos industriales, impermeabilización de losas, reservorios, muros de contención y áreas sujetas a infiltración, proyectos paisajísticos, impermeabilización de túneles, tanques de procesos de lixiviación y / o decantación, impermeabilización del suelo para lixiviación en baterías y canaletas, procesos generales de minería, etc.</p>`
          },
          {
            title: 'Geomembrana de polietileno HDPE',
            href: '',
            description: `<p>Las geomembranas de polietileno de alta densidad se  fabrican con resinas de alto peso molecular certificadas por sus  suministradores.&nbsp; Se aplican en  condiciones químicas y físicas severas como rellenos sanitarios e industriales, saneamiento básico, almacenamiento y tratamiento de agua y de efluentes  industriales, minería y otras actividades relacionadas al medio ambiente.</p>
                          <p>Se utilizan también en el recubrimiento de  pozos y depósitos de agua y desechos, estanques para el almacenamiento de agua, contención secundaria, canales de irrigación y otros sistemas de revestimiento  y contención.< br >
                          <p>El control de calidad de las geomembranas que  ofrecemos sigue las recomendaciones de la GM-13 del Geosynthetic Research  Institute.</p>`
          },
          {
            title: 'LLDPE',
            href: '',
            description: ''
          },
          {
            title: 'Geomembranas PVC',
            href: '',
            description: ''
          },
        ]
      },
      {
        title: 'Geocompuestos',
        href: 'geocompuestos',
        src: 'images/geocompuestos-400px.jpg',
        subsubmenu: [
          {
            title: 'Geomallas',
            href: ''
          },
          {
            title: 'Geogrillas',
            href: ''
          }
        ]
      },
      {
        title: 'Geotubos',
        href: 'geotubos',
        src: 'images/geotubos-400px.jpg',
        subsubmenu: [
          {
            title: 'Control de erosión',
            href: 'images/no_imagen.png',
            src: 'images/no_imagen.png'
          },
          {
            title: 'Biomantas para el control de erosión',
            href: 'images/no_imagen.png',
            src: 'images/no_imagen.png'
          }
        ],
        gallery: [
          {
            title: 'Suministro de Geotubos, Río Grande, Santa Cruz, 2016',
            thumb: 'images/galerias/geotubos01-250px.jpg',
            full: 'images/galerias/geotubos01-700px.jpg'
          },
          {
            title: 'Suministro de Geotubos, Río Grande, Santa Cruz, 2016',
            thumb: 'images/galerias/geotubos02-250px.jpg',
            full: 'images/galerias/geotubos02-700px.jpg'
          },
          {
            title: 'Suministro de Geotubos, Río Grande, Santa Cruz, 2016',
            thumb: 'images/galerias/geotubos03-250px.jpg',
            full: 'images/galerias/geotubos03-700px.jpg'
          },
          {
            title: 'Suministro de Geotubos, Río Grande, Santa Cruz, 2016',
            thumb: 'images/galerias/geotubos04-250px.jpg',
            full: 'images/galerias/geotubos04-700px.jpg'
          },
          {
            title: 'Suministro de Geotubos, Río Grande, Santa Cruz, 2016',
            thumb: 'images/galerias/geotubos05-250px.jpg',
            full: 'images/galerias/geotubos05-700px.jpg'
          }
        ]
      },
      {
        title: 'Servicio de impermeabilización',
        href: 'servicio-de-impermeabilizacion',
        src: 'images/servicio-de-impermeabilizacion-400px.jpg',
        subsubmenu: [
          {
            title: 'Geomembranas de HDPE',
            href: ''
          },
          {
            title: 'Provisión e instalación',
            href: ''
          }
        ]
      },
      {
        title: 'Sistemas de control de inundaciones',
        href: 'sistemas-de-control-de-inundaciones',
        src: 'images/sistemas-de-control-de-inundaciones-400px.jpg',
      }
    ]
  },
  {
    title: 'Tuberías de hdpe y acero',
    href: 'tuberias-de-hdpe-y-acero',
    state: false,
    submenu: [
      {
        title: 'Tuberías de corrugada de drenaje HDPE',
        href: 'tuberias-de-corrugada-de-drenaje-HDPE',
        src: 'images/tuberiasHDPE-400px.jpg',
        gallery: [
          {
            title: 'Suministro de Geotubos, Río Grande, Santa Cruz, 2016',
            thumb: 'images/galerias/geotubos01-250px.jpg',
            full: 'images/galerias/geotubos01-700px.jpg'
          },
          {
            title: 'Suministro de Geotubos, Río Grande, Santa Cruz, 2016',
            thumb: 'images/galerias/geotubos02-250px.jpg',
            full: 'images/galerias/geotubos02-700px.jpg'
          }
        ]
      },
      {
        title: 'Tuberías de drenaje de HPDE y de acero',
        href: 'tuberias-de-drenaje-de-HPDE-y-de-acero',
        src: 'images/no_imagen.png',
      }
    ]
  },
  {
    title: 'Válvulas y bombas',
    href: 'valvulas-y-bombas',
    state: false,
    submenu: [
      {
        title: 'Válvulas de control',
        href: 'valvulas-de-control',
        src: 'images/no_imagen.png'
      },
      {
        title: 'Válvulas de operación',
        href: 'valvulas-de-operacion',
        src: 'images/no_imagen.png'
      }
    ]
  },
  {
    title: 'Generadores de energía',
    href: 'generadores-de-energia.html',
  },
  {
    title: 'Macromedición',
    href: 'macromedicion',
    state: false,
    submenu: [
      {
        title: 'Medición de combustibles',
        href: 'images/no_imagen.png',
        src: 'images/no_imagen.png',
      }
    ]
  },
  {
    title: 'Otros productos',
    href: 'otros-productos',
    state: false,
    submenu: [
      { 
        title: 'Guardavías(Flex Bean)',
        href: 'guardavias',
        src: 'images/no_imagen.png'
      },
      { 
        title: 'Bolsacretos',
        href: 'bolsacretos',
        src: 'images/no_imagen.png'
      },
      { 
        title: 'Mirasafe',
        href: 'mirasafe',
        src: 'images/no_imagen.png'
      },
      { 
        title: 'Anclaje de HPDE',
        href: 'anclaje-de-hpde',
        src: 'images/no_imagen.png'
      },
      { 
        title: 'Revestimientos de Arcilla Geosintética',
        href: 'revestimientos-de-arcilla-geosintetica',
        src: 'images/no_imagen.png'
      },
      { 
        title: 'Aljibes para almacenamiento de agua',
        href: 'aljibes-para-almacenamiento-de-agua',
        src: 'images/no_imagen.png',
        description: 'Fabricación tanques de 5000, 10000 y 20000 litros de capacidad, portatiles',
        gallery: [
          {
            title: 'Aljibes / Tanques Flexibles de Almacenamiento de Agua',
            thumb: 'images/galerias/aljibes01-250px.jpg',
            full: 'images/galerias/aljibes01-700px.jpg'
          },
          {
            title: 'Aljibes / Tanques Flexibles de Almacenamiento de Agua',
            thumb: 'images/galerias/aljibes02-250px.jpg',
            full: 'images/galerias/aljibes02-700px.jpg'
          },
          {
            title: 'Aljibes / Tanques Flexibles de Almacenamiento de Agua',
            thumb: 'images/galerias/aljibes03-250px.jpg',
            full: 'images/galerias/aljibes03-700px.jpg'
          },
          {
            title: 'Aljibes / Tanques Flexibles de Almacenamiento de Agua',
            thumb: 'images/galerias/aljibes04-250px.jpg',
            full: 'images/galerias/aljibes04-700px.jpg'
          }
        ]
      }
    ]
  },
  {
    title: 'Proyectos',
    href: 'proyectos.html'
  },
  {
    title: 'Informacion Técnica',
    href: 'informacion-tecnica.html',
    docs: [
      {
        title: 'Manual de Control de Calidad y  Seguridad Cualitativa de MARIENCO SRL',
        href: 'docs/Manual de Control de Calidad y  Seguridad Cualitativa de MARIENCO SRL',
        type: 'pdf'
      },
      {
        title: 'Certificado IAGI',
        href: 'docs/Certificado IAGI',
        type: 'pdf'
      },
      {
        title: 'Catalogo de Productos : Geogrillas',
        href: 'docs/Catalogo de Productos : Geogrillas',
        type: 'pdf'
      },
      {
        title: 'Especificaciones Tecnicas  Geogrillas Reforzamiento de Suelos',
        href: 'docs/Especificaciones Tecnicas  Geogrillas Reforzamiento de Suelos',
        type: 'pdf'
      },
      {
        title: 'Especificaciones Tecnicas  Geotextiles No Tejidos Linea Minera M',
        href: 'docs/Especificaciones Tecnicas  Geotextiles No Tejidos Linea Minera M',
        type: 'pdf'
      },
      {
        title: 'Catalogo de Productos : Geotubos',
        href: 'docs/Catalogo de Productos : Geotubos',
        type: 'pdf'
      },
      {
        title: 'Catalogo de Productos :  Bolsacretos',
        href: 'docs/Catalogo de Productos :  Bolsacretos',
        type: 'pdf'
      },
      {
        title: 'Especificaciones Tecnicas  Geotextiles NT2000M',
        href: 'docs/Especificaciones Tecnicas  Geotextiles NT2000M',
        type: 'pdf'
      },
      {
        title: 'Especificaciones Tecnicas  Geotextiles NT2675M',
        href: 'docs/Especificaciones Tecnicas  Geotextiles NT2675M',
        type: 'pdf'
      },
      {
        title: 'Catalogo de Productos : Válvulas  de Control',
        href: 'docs/Catalogo de Productos : Válvulas  de Control',
        type: 'pdf'
      },
      {
        title: 'Especificaciones Tecnicas  Geotextiles NT4000',
        href: 'docs/Especificaciones Tecnicas  Geotextiles NT4000',
        type: 'pdf'
      },
      {
        title: 'Especificaciones Tecnicas  Geotextiles NT4150M',
        href: 'docs/Especificaciones Tecnicas  Geotextiles NT4150M',
        type: 'pdf'
      },
      {
        title: 'Catalogo de Productos : Introducción  a los Geotextiles',
        href: 'docs/Catalogo de Productos : Introducción  a los Geotextiles',
        type: 'pdf'
      },
      {
        title: 'Presentación División Geosintéticos',
        href: 'docs/Presentación División Geosintéticos',
        type: 'ppt'
      },
      {
        title: 'Especificaciones Tecnicas  Geotextiles NT5000',
        href: 'docs/Especificaciones Tecnicas  Geotextiles NT5000',
        type: 'pdf'
      },
      {
        title: 'Especificaciones Tecnicas  Geotextiles NT5000M',
        href: 'docs/Especificaciones Tecnicas  Geotextiles NT5000M',
        type: 'pdf'
      },
      {
        title: 'Especificaciones Tecnicas  Geotextiles NT7350M',
        href: 'docs/Especificaciones Tecnicas  Geotextiles NT7350M',
        type: 'pdf'
      },
      {
        title: 'Especificaciones Tecnicas  Geotextiles NT8000M',
        href: 'docs/Especificaciones Tecnicas  Geotextiles NT8000M',
        type: 'pdf'
      },
      {
        title: 'Especificaciones Tecnicas  Geotextiles Tejidos y No Tejidos',
        href: 'docs/Especificaciones Tecnicas  Geotextiles Tejidos y No Tejidos',
        type: 'pdf'
      },
      {
        title: 'Especificaciones técnicas Geomembranas HPDE',
        href: 'docs/Especificaciones técnicas Geomembranas HPDE',
        type: 'pdf'
      }
    ]
  }
]
