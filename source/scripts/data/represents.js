export const represents = [
  {
    title: 'Cetco',
    src: 'images/representantes/cetco.jpg',
    href: "http://www.cetco.com/es-mx/  "
  },
  // {
  //   title: 'iagi',
  //   src: 'images/representantes/iagi.jpg',
  //   href: "/"
  // },
  {
    title: 'Novacero',
    src: 'images/representantes/novacero.jpg',
    href: "http://www.novacero.com"
  },
  {
    title: 'Total Control Systems, Inc',
    src: 'images/representantes/tsc.jpg',
    href: "http://www.tcsmeters.com"
  },
  {
    title: 'Cla-Val, Co.',
    src: 'images/representantes/claval.jpg',
    href: "https://www.cla-val.com"
  },
  {
    title: 'Inmesol Power Solutions',
    src: 'images/representantes/inmesol.jpg',
    href: "http://www.inmesol.es"
  },
  {
    title: 'Pavco Geosinteticos',
    src: 'images/representantes/pavco.jpg',
    href: "https://pavco.com.co"
  },
  {
    title: 'Cornell Pump Systems, Inc',
    src: 'images/representantes/cornell.jpg',
    href: "https://www.cornellpump.com"
  },
  {
    title: 'Kanaflex',
    src: 'images/representantes/kanaflex.jpg',
    href: "http://www.kanaflexcorp.com"
  },
  {
    title: 'Procables SA CI',
    src: 'images/representantes/procables.jpg',
    href: "http://procables.com.co"
  },
  {
    title: 'deflor Bioengenharia',
    src: 'images/representantes/deflor.jpg',
    href: "http://deflor.com.br"
  },
  {
    title: 'Mirafi Construction Products',
    src: 'images/representantes/mirafi.jpg',
    href: "https://www.tencategeo.us"
  },
  {
    title: 'Tenax',
    src: 'images/representantes/tenax.jpg',
    href: "http://www.tenaxus.com"
  },
]