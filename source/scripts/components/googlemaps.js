export const googleMap = {
  template: `<div class="google-map" :id="mapName"></div>`,
  data () {
    return {
      mapName: 'map',
      zoom: 16,
      markerCoordinates: [{
        latitude: -17.771430,
        longitude: -63.185969,
        title: `<div id="content">
                    <h2 id="firstHeading" class="firstHeading">Marienco - Tecnología y control</h2>
                    <div id="bodyContent">
                      <p><b>Descripción:</b> Instalación e insumos geosintéticos, tuberías de HDPE y acero.</p>
                      <p><b>Dirección:</b> Edificio La Casona<br>Oficina N° 120, Avenida Cristóbal de Mendoza N° 246<br>Telefono: +591 3-341-1929 / Celular: +591 708-90500<br>E-mail: info@marienco.com</p>
                    </div><hr>
                    <footer><a class="button--cta center" href="https://www.google.com.bo/maps/place/Condominio+La+Casona/@-17.7714296,-63.1881576,17z/data=!3m1!4b1!4m5!3m4!1s0x93f1e7e3ddf3dfdd:0x506015b70fb77025!8m2!3d-17.7714296!4d-63.1859689?hl=es" target="_blank">Ver en google maps</a></footer>
                  </div>`
      }]
    }
  },
  mounted () {
    const element = document.getElementById(this.mapName)
    const options = {
      zoom: this.zoom,
      center: new google.maps.LatLng(this.markerCoordinates[0].latitude, this.markerCoordinates[0].longitude)
    }
    const map = new google.maps.Map(element, options);
    
    this.markerCoordinates.forEach((coord) => {
      const position = new google.maps.LatLng(coord.latitude, coord.longitude);
      const marker = new google.maps.Marker({
        position,
        map
      });
      
      const infowindow = new google.maps.InfoWindow()
      marker.addListener('click', function () {
        infowindow.close();
      });
      google.maps.event.addListener(marker, 'click',
      (function (marker, coord, infowindow) {
        return function () {
          infowindow.setContent(coord.title);
          infowindow.open(map, marker);
        };
      })(marker, coord, infowindow));
    });
  }
}