const gulp = require('gulp');
const log = require('fancy-log');
const responsive = require('gulp-responsive-images');
const imagemin = require('gulp-imagemin');
const colors = require('ansi-colors');
const count = require('gulp-count');

// load config
const config = require('../config');

const task = () => gulp.src(config.resize.sourceFiles)

  .pipe(responsive({
    // Resize all JPG images to three different sizes: 200, 500, and 630 pixels
    '*.{jpg,png,jpeg,gif,webp}': [
    {
      // width: 400,
      height: 400,
      rename: { suffix: '-400px' },
    }],
    'galerias/*.{jpg,png,jpeg,gif,webp}': 
      [
        {
          // width: 400,
          height: 250,
          rename: { suffix: '-250px' },
        },
        {
          // width: 400,
          height: 700,
          rename: { suffix: '-700px' },
        },
      ],
  }, {
    // Global configuration for all images
    // The output quality for JPEG, WebP and TIFF output formats
    quality: 70,
    // Use progressive (interlace) scan for JPEG and PNG output
    progressive: true,
    // Strip all metadata
    withMetadata: false,
  }))
  // minify (production)
  .pipe(imagemin([
    // plugins (https://www.npmjs.com/browse/keyword/imageminplugin)
    imagemin.gifsicle(),
    imagemin.jpegtran(),
    imagemin.optipng(),
    // imagemin.svgo()
  ], {
      // options
      verbose: true
    }))

  // log
  .pipe(count({
    message: colors.white('Resize image files processed: <%= counter %>'),
    logger: (message) => log(message)
  }))

  // save
  .pipe(gulp.dest(config.resize.destinationFolder));

gulp.task('resize', task);
module.exports = task;